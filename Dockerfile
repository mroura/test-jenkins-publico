FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install python3 python3-pip -y
RUN pip3 install Flask 
RUN mkdir /opt/app
COPY src/* /opt/app
COPY docker-entrypoint.sh /
ENTRYPOINT "/docker-entrypoint.sh"